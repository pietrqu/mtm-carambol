<?php

namespace AppBundle\Form;

use AppBundle\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class TaskType extends AbstractType
{
		public function buildForm(FormBuilderInterface $builder, array $options)
		{
//				$builder->add(
//					'category',
//					CategoryType::class
//				);
				
				$builder
					->add('description');
				$builder
					->add(
						'tags',
						CollectionType::class,
						[
							'entry_type'    => TagType::class,
//							'entry_options' => array('label' => false),
								'label' => 'dupa nie rozwiazanie'
						]
					);
				
				$builder->add('save', SubmitType::class, array(
					'attr' => array('class' => 'save'),
				));

		}
		
		public function configureOptions(OptionsResolver $resolver)
		{
				$resolver->setDefaults(
					[
						'data_class' => Task::class,
					]
				);
		}
		
		public function getBlockPrefix()
		{
				return 'app_bundle_task_type';
		}
}

<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Task
 * @package AppBundle\Entity
 *          @ORM\Entity()
 *          @ORM\Table()
 */
class Task
{
		/**
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 * @ORM\Column(type="integer")
		 */
		protected $id;
		
		/**
		 * @ORM\Column(type="string")
		 */
		protected $description;
		
		/**
		 * @ORM\OneToMany(targetEntity="AppBundle\Entity\Tag", mappedBy="task")
		 * @ORM\Column(type="string")
		 */
		protected $tags;
		
		public function __construct()
		{
				$this->tags = new ArrayCollection();
		}
		
		public function getDescription()
		{
				return $this->description;
		}
		
		public function setDescription($description)
		{
				$this->description = $description;
		}
		
		public function getTags()
		{
				return $this->tags;
		}
		
		/**
		 * @param ArrayCollection $tags
		 */
		public function setTags($tags)
		{
				$this->tags = $tags;
		}
		
		/**
		 * @return mixed
		 */
		public function getId()
		{
				return $this->id;
		}
	
	public function addTags($tags)
	{
		return $this->tags[] = $tags;
		
	}
		
}
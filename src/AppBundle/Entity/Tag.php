<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tag
 * @package AppBundle\Entity
 *          @ORM\Entity()
 *          @ORM\Table()
 */
class Tag
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;
	
	/**
	 * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Task")
	 */
	protected $task;
	
		public function getName()
		{
				return $this->name;
		}
		
		public function setName($name)
		{
				$this->name = $name;
		}
}
<?php


namespace AppBundle\Controller;


use AppBundle\Entity\Tag;
use AppBundle\Entity\Task;
use AppBundle\Form\TaskType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends Controller
{
		/**
		 * @param Request $request
		 * @return \Symfony\Component\HttpFoundation\Response
		 * @Route("/new-task", name="add_new_task")
		 */
		public function newAction(Request $request)
		{
				$task = new Task();
				
						$tag1 = new Tag();
						$tag1->setName('tag1');
				
				$task->getTags()->add($tag1);
				
				
						$tag2 = new Tag();
						$tag2->setName('tag2');
						
				$task->getTags()->add($tag2);
				
				//$task->getTags()->add($tag2);
				
				$form = $this->createForm(TaskType::class, $task);
				$form->handleRequest($request);
				
				if ($form->isValid() && $form->isSubmitted()) {
					
					$data = $form->getData();
					$data->addTags($request);
					
					$em = $this->getDoctrine()->getManager();
					
					$em->persist($data);
					$em->flush();
						
						
						
						dump($data);
				
				}
				
				return $this->render(
					'task/new.html.twig',
					[
						'form' => $form->createView(),
					]
				);
		}
}
    